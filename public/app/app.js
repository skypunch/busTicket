angular.module('busTicket',[])
.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
})
.controller('homeController',['$scope','$location','$timeout',function(scope,location,timeout){
  // access dice a la vista si muestra el formulario de login o el la lista de enlaces
  scope.access=false;
  // si no se ha iniciado sesion
  if(sessionStorage.user!= null && sessionStorage.user!=''){
    // se abre una conexion con el manejador de corridas disponibles
    var socket = io.connect(location.protocol()+'://'+location.host()+':'+location.port()+'/socket/bus/available', { 'forceNew': true });
    // se marca access como true para que la vista mueste los enlaces
    scope.access=true;
    scope.buses=[];
    // esta variable mostrara texto en la vista /
    scope.stadistic='estadistica';
    // cada que se detecte el evento disponibles se actualizaran todas las corridas de los proximos 5 dias
    socket.on('disponibles', function(data) {
      scope.buses=data;
      timeout(function(){
        console.log(scope.buses);
      },20);
    })
  }
  // si se inicio sesion
  else{
    // se abre un socket hacia /signIn
    var socket = io.connect(location.protocol()+'://'+location.host()+':'+location.port()+'/signIn', { 'forceNew': true });
    scope.sign={
      usuario:'',
      contrasena:''
    };
    // cuando se presione el boton entrar se emite el evento autenticar y se manda la informacion del formulario
    scope.send=function(){
      socket.emit('autenticar',scope.sign);
    }
    // si se da el evento permitido se guarda la session con sessionStorage y se cierra el socket de signIn
    socket.on('permitido',(row)=>{
      console.log(row);
      sessionStorage.user=row.id_usuario;
      sessionStorage.name=row.nombre;
      socket.disconnect();
      // se recarga la pagina
      window.location.reload();
    });
    // si se da el evento denegado lanza un alert con el texto que mando el servidor
    socket.on('denegado',(message)=>{
      alert(message.msg);
    })
  }
}])
.controller('busController',['$scope','$location','$timeout',function(scope,location,timeout){
  // si se a iniciado sesion
  if(sessionStorage.user!= null && sessionStorage.user!=''){
    // se obtine el id de la corrida
    scope.busID=window.location.pathname.split('/')[2];
    var socket = io.connect(location.protocol()+'://'+location.host()+':'+location.port()+'/socket/bus/'+scope.busID, { 'forceNew': true });
    // si se da el evento lugares se actualizan los lugares de autobus
    socket.on('lugares', function(lugares) {
      scope.lugares=lugares;
      timeout(function(){
        console.log(scope.lugares);
      },100);
    })
    socket.on('boleto',function(boleto){
      scope.boleto=boleto;
      timeout(function(){
        console.log(scope.boleto);
      },100);
    })
    scope.pedir=function(numeroLugar){
      if( scope.lugares.lista[numeroLugar-1].estatus==='ocupado'){
        socket.emit('pedir boleto', scope.lugares.lista[numeroLugar-1]);
      }
      else{
        alert('No Puede solicitar un boleto que no se a vendido');
      }
    }
    // funcion seleccionar
    scope.seleccionar=function(numeroLugar) {
      // se guarda de manera local el lugar con el que se va a trabaar
      var lugarActual = scope.lugares.lista[numeroLugar-1];
      switch (lugarActual.estatus) {
        // si el lugar esta disponible se pone como reservado (esta informacion no es persistente)
        case 'disponible':
          lugarActual.estatus='reservado';
          lugarActual.usuario=Number(sessionStorage.user);
          // se emite el evento actualizacion
          socket.emit('actualizacion',{index:numeroLugar-1,lugar:lugarActual});
          break;
        // si el lugar ya fue reservado
        case 'reservado':
        // y fue reservado por este usuario
          if(lugarActual.usuario===Number(sessionStorage.user)){
            // se pone como ocupado y se pide el nombre de la persona que usara el lugar
            lugarActual.estatus='ocupado';
            lugarActual.nombre=prompt('Nombre del titular');
            if(lugarActual.nombre!=null){
              // si se paso infomacion se emite el evento actualizacion
              socket.emit('actualizacion',{index:numeroLugar-1,lugar:lugarActual});
            }
            else{
              // de lo contrario se pone como disponible
              lugarActual.estatus='disponible';
              lugarActual.nombre='';
              lugarActual.usuario=null;
              socket.emit('actualizacion',{index:numeroLugar-1,lugar:lugarActual});
            }
          }
          else{
            // si el asiento fue reservado pr algun otro vendedor
            alert('Este asiento fue reservado por otro vendedor');
          }
          break;
        case 'ocupado':
          // si este usuario vendio
          if(lugarActual.usuario===Number(sessionStorage.user)){
            if(confirm('Desea eliminar esta compra?')){
              // y se acepta la cancelacion se pone a disponible y se emite el evento actualizar
              lugarActual.estatus='disponible';
              lugarActual.nombre='';
              socket.emit('actualizacion',{index:numeroLugar-1,lugar:lugarActual});
            }
            break;
          }
          else{
            // si este usuario no realizo la venta no le permite la cancelacion
            alert('Este asiento fue asignado por otro vendedor, usted no puede hacer la cancelación');
          }
        default:
          console.log("Este estatus no es valido");
      }
    }
  }
  else{
    // si no hay session iniciada manda a home
     window.location='/';
  }
}])
.controller('stadisticController',['$scope','$location','$timeout',function(scope,location,timeout){
  var lugares = io.connect(location.protocol()+'://'+location.host()+':'+location.port()+'/socket/bus/list', { 'forceNew': true });
  scope.lugares=[];
  scope.corridas=[];
  scope.stadistic='estadistica';
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable(scope.corridas);
    var options = {
      chart: {
        title: 'Asientos Ocupados',
        subtitle: '(Para cada corrida proxima a salir)',
      },
      legend: { position: 'none' },
      bar: { groupWidth: '95%' },
      animation:{
        duration: 1000,
        easing: 'out',
      },
      vAxis: {minValue:0, maxValue:45, count: 1, scaleType: 'mirrorLog'},
    };
    var mainChart = new google.charts.Bar(document.getElementById('main_chart'));
    mainChart.draw(data, options);
  }
  lugares.on('lugares', function(data) {
    scope.lugares=data;
    scope.corridas=[];
    scope.corridas.push(['Corrida','Asientos Ocupados']);
    for (corrida of Object.keys(scope.lugares)) {
      scope.corridas.push([corrida,45-scope.lugares[corrida].disponibles]);
    }
    timeout(function(){
      console.log(scope.corridas);
      console.log(scope.lugares);
      drawChart()
    },100);
  })
}])
