# busTicket
v0.1.0
Es una aplicacion para venta de boletos de autobus basada en Node.js y Socket.io

Desarrollado por @skypunch

## Bibliotecas y dependencias

* ___Backend___
  - __Node.js__ (Es el entorno de ejecución de la aplicación)
  - __Socket.io__ (Es el gestor de sockets TCP de la aplicación)
  - __Espress.js__ (Permitemontar el servidor HTTP para gestinar las vistas)
  - __Mysql2__ (Es el conector a la Base de datos)
* ___Fronend___
  - __Angular.js__ (Gestiona la aplicación y en este caso se usa para pasar el valor de las variables al html o visversa)
  - __jQuery__ (dependencia de Bootstrap)
  - __Bootstrap__ (Es la biblioteca usada para darle vista o estilo a la aplicación)
  - __Google Charts__ (Utilizada para mostrar la grafica en estadistica)

## Instalación

### Descarga

(Necesita git)

``` bash
git clone https://gitlab.com/skypunch/busTicket.git
```

(inmediatamente despues de descargar)

``` bash
cd busTicket
```

Para descargar las dependencias del proyecto

``` bash
npm install
```

### Configuración de la Base de datos

(Necesita MySQL)
Iniciar MySQL (Sustituya USUARIO y PASSWORD por sus datos de acceso)

``` bash
mysql -uUSUARIO -pPASSWORD
```

(ejemplo)
``` bash
 mysql -uroot -pskypunch
```
Una vez dentro de mysql se indica la ruta absoluta del archivo sql que contiene la base de datos
(ejemplo, USUARIO hace referencia al usuario del sistema no de MySQL)
```sql
source /home/USUARIO/busTicket/db/busTicketDB.sql
```

En la carpeta raiz del proyecto, se encuentra el archivo 'server.js', cambie las credenciales de autenticacion para la base de datos (Line 7: const DBoptions..)
``` javascript
const DBoptions={
  host     : 'HOST',
  user     : 'USUARIO',
  password : 'PASSWORD',
  database : 'busTicketDB'
}
```

### Ejecución

Desde terminal (dentro de la carpeta raiz del proyecto)
``` bash
node .
```

## Uso

Abra el navegador en la ruta
```curl
http://localhost:3000
```
o  Utilize su dirección ip local para acceder desde otro equipo dentro de la misma red local

### acceso
Use cualquiera de los usuarios predefinidos o ingrese nuevos en db/busTicketDB.sql
```sql
INSERT INTO `busTicketDB`.`usuarios`
(`nombre`, `contraseña`) VALUES
('taquilla1', 'taquilla01'),
('taquilla2', 'taquilla02'),
('taquilla3', 'taquilla03'),
('taquilla4', 'taquilla04'),
('taquilla5', 'taquilla05');
```

## Archivos Principales
* ___server.js___: Servidor de la aplicación
* ___app.js___: Gestiona el frontend con ayuda de Angular.js

## TODO
- [x] Funciones
    - [x] Estadistica
    - [x] Venta
    - [x] Boletos
- [ ] Impresión de boletos
