DROP SCHEMA IF EXISTS `busTicketDB`;
CREATE SCHEMA `busTicketDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
use `busTicketDB`;
CREATE TABLE `busTicketDB`.`usuarios` (
	`id_usuario` SERIAL,
	`nombre` varchar(64) not null unique,
	`contraseña` varchar(64) not null,
	`creado` timestamp default CURRENT_TIMESTAMP,
PRIMARY KEY (`id_usuario`));
CREATE TABLE `busTicketDB`.`autobuses` (
	`id_autobus` SERIAL,
	`nombre` varchar(64) not null unique key,
	`lugares` int(4) unsigned default 45,
	`creado` timestamp default CURRENT_TIMESTAMP,
PRIMARY KEY (`id_autobus`));
CREATE TABLE `busTicketDB`.`destinos` (
	`id_destino` SERIAL,
	`nombre` varchar(64) not null unique key,
	`descripcion` text,
	`creado` timestamp default CURRENT_TIMESTAMP,
PRIMARY KEY (`id_destino`));
CREATE TABLE `busTicketDB`.`corridas` (
	`id_corrida` SERIAL,
	`autobus` bigint unsigned not null,
	`destino` bigint unsigned not null,
	`costo` real not null,
	`salida` datetime not null,
	`creado` timestamp default CURRENT_TIMESTAMP,
PRIMARY KEY (`id_corrida`),
CONSTRAINT `fk_autobuses`
FOREIGN KEY (`autobus`) REFERENCES autobuses(`id_autobus`)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT `fk_destinos`
FOREIGN KEY (`destino`) REFERENCES destinos(`id_destino`)
ON DELETE CASCADE
ON UPDATE CASCADE);
CREATE TABLE `busTicketDB`.`boletos` (
	`id_boleto` SERIAL,
	`usuario` bigint unsigned not null,
	`corrida` bigint unsigned not null,
	`nombre` varchar(64) not null,
	`asiento` int(3) not null,
	`creado` timestamp default CURRENT_TIMESTAMP,
PRIMARY KEY (`id_boleto`),
CONSTRAINT `fk_usuarios`
FOREIGN KEY (`usuario`) REFERENCES usuarios(`id_usuario`)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT `fk_corridas`
FOREIGN KEY (`corrida`) REFERENCES corridas(`id_corrida`)
ON DELETE CASCADE
ON UPDATE CASCADE);

INSERT INTO `busTicketDB`.`usuarios`
(`nombre`, `contraseña`) VALUES
('taquilla1', 'taquilla01'),
('taquilla2', 'taquilla02'),
('taquilla3', 'taquilla03'),
('taquilla4', 'taquilla04'),
('taquilla5', 'taquilla05');

INSERT INTO `busTicketDB`.`autobuses`
(`nombre`) VALUES
('bus01'),
('bus02'),
('bus03'),
('bus04'),
('bus05'),
('bus06');

INSERT INTO `busTicketDB`.`destinos`
(`nombre`, `descripcion`) VALUES
('Guadalajara', '---'),
('Monterrey', '---'),
('CD Juarez', '---'),
('Toluca', '---'),
('Acapulco', '---'),
('Sinaloa', '---');
-- NOW() + INTERVAL 1 DAY

INSERT INTO `busTicketDB`.`corridas`
(`autobus`, `destino`, `costo`,`salida`) VALUES
(1,6,980.5,NOW() + INTERVAL 2 DAY),
(2,5,720.0,NOW() + INTERVAL 2 DAY),
(3,1,1020.2,NOW() + INTERVAL 2 DAY),
(4,2,680.5,NOW() + INTERVAL 2 DAY),
(5,4,855.0,NOW() + INTERVAL 2 DAY),
(6,3,1000.5,NOW() + INTERVAL 2 DAY);
