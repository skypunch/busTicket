(()=>{
  // si el servicio esta sobre un servidor usar su puerto, de lo contrario usar el 3000
  const PORT = process.env.port || 3000;
  // conector a mysql
  const mysql = require('mysql2');
  // datos requeridos para la coneccion
  const DBoptions={
    host     : 'localhost',
    user     : 'root',
    password : 'skypunch',
    database : 'busTicketDB'
  }
  let connection = mysql.createConnection(DBoptions);
  connection.config.namedPlaceholders = true;

  // Se llama a express
  let express = require('express');
  // path es una biblioteca del core de NODE.JS que permite trabajar con rutas del sistema
  let path = require('path');
  // se instancia express en la variable local app
  let app = express();
  // se crea un servidor http compatible con sockets
  let server = require('http').Server(app);
  // se monta el socket sobre el servidor
  let io = require('socket.io')(server);

  // Se establece la ruta views para que se envien vistas a partir de ella
  app.set('views', __dirname + '/views');
  // Se establece la ruta root como raiz del proyecto
  app.set('root', __dirname + '/public');
  // se define que la carpeta public sera la que se mostrara en el servidor http
  app.use(express.static('public'));
  // manda el rchivo index.html cuando la ruta es /
  app.route('/')
  .get((req,res)=>{
    res.status(200)
    .type('html')
    .sendFile(path.join(app.get('root')+'/index.html'));
  });
  // manda el archivo resumeBuses.html cuando la ruta es /estadistica
  app.route('/estadistica')
  .get((req,res)=>{
    res.status(200)
    .type('html')
    .sendFile(path.join(app.get('views')+'/resumeBuses.html'));
  });
  // manda el archivo singleBus cuando la ruta es /autobus/:bus_id
  app.route('/autobus/:bus_id')
  .get((req,res)=>{
    res.status(200)
    .type('html')
    .sendFile(path.join(app.get('views')+'/singleBus.html'));
  });

  // manejador de sockets para inicio de sesion
  let signIn=io.of('/signIn')
  //al conectar
  .on('connection',(socket)=>{
    //imprime este mensaje
    console.log('Se conecto un nuevo cliente a /signIn');
    // cuando el cliente emite el evento 'autenticar'
    socket.on('autenticar',(data)=>{
      // lo busca en la base de datos
      connection.query('select id_usuario, nombre from usuarios where nombre=:usuario AND contraseña=:contrasena limit 1',data,function(err, rows, fields){
        if(rows && rows.length>0){
          // si coincide se emite el evento 'permitido' con la informacion que se recupero de la BD
          socket.emit('permitido',rows[0]);
        }
        else{
          // si no coinciden los datos emite el evento 'denegado' con un mensaje informativo
          socket.emit('denegado',{msg:"No se permite el acceso"});
        }
      })
    })
    // al desconectar
    socket.on('disconnect',()=>{
      // imprime este mensaje
      console.log('Se desconecto un cliente de /signIn');
    })
  });
  // este query trae el numero de boletos vendidos por cada corrida
  mysql.createConnection(DBoptions).query("select b.corrida, count(b.id_boleto) 'vendidos' from boletos as b LEFT JOIN (corridas as c) ON (c.id_corrida=b.corrida AND c.salida BETWEEN NOW() AND (NOW() + INTERVAL 5 DAY)) group by b.corrida  order by c.salida desc",//{corrida:b.id_corrida},
  function(err,rows){
    if(err) console.log( err.stack);
    // se guardan los resultados o un arreglo basio si no los hubo
    let boletosVendidos=rows || [];
    // este query trae el detalle de cada boleto (toda la información)
    mysql.createConnection(DBoptions).query("SELECT b.id_boleto,b.nombre,b.asiento,d.nombre 'destino',b.corrida, c.costo,c.salida,u.nombre 'emisor',b.usuario 'id_vendedor', a.nombre 'autobus', c.autobus 'id_autobus' FROM boletos as b LEFT JOIN (corridas as c,destinos as d,usuarios as u, autobuses as a) ON (c.id_corrida=b.corrida AND c.destino=d.id_destino AND u.id_usuario=b.usuario AND a.id_autobus=c.autobus AND c.salida BETWEEN NOW() AND (NOW() + INTERVAL 5 DAY)) order by c.salida desc;",function(err,rows){
      if(err) console.log( err.stack);
        // se guarda en el objeto detalle los resultados o un arreglo basio si no los hubo
      boletosVendidos.detalle=rows || [];
      // este query trae la lista de autobuses-destino para los proximos 5 dias
      mysql.createConnection(DBoptions).query("select c.id_corrida,d.id_destino,a.id_autobus,d.nombre 'destino', c.costo,a.nombre, a.lugares 'asientos',c.salida from corridas as c LEFT JOIN (autobuses as a,destinos as d) ON ( a.id_autobus=c.autobus AND d.id_destino=c.destino AND c.salida BETWEEN NOW() AND (NOW() + INTERVAL 5 DAY)) order by c.salida desc;", function(err, rows, fields) {
        console.time('inicio en ');
        if (err) console.log(err.stack);
        let buses = rows || [];
        // Objeto que contendra todos los manejadores de sockets
        let bus={
          //Manejador de estadistica
          list:io.of('/socket/bus/list')
          .on('connection',(socket)=>{
            console.log('Se conecto un nuevo cliente a /bus/list');
            socket.emit('lugares',lugares);
            socket.on('disconnect',()=>{
              console.log('Se desconecto un cliente a /bus/list');
            })
          }),
          // manejador de todos los autobuses-destino (corridas) disponibles
          disponibles:io.of('/socket/bus/available')
          .on('connection',(socket)=>{
            console.log('Se conecto un nuevo cliente a /bus/available');
            // cada que se abre una conexion se envia la lista de corridas mediante el evento 'disponibles'
            socket.emit('disponibles',buses);
            socket.on('disconnect',()=>{
              console.log('Se desconecto un cliente a /bus/available');
            })
          })
        }
        let lugares = {};
        // creacion dinamica de un manejador de socket por cada autobus
        // ciclo para iterar sobre el objeto que contiene las corridas
        for (b of buses) {
          lugares[b.destino+'-'+b.nombre]={
            lista:[],
            disponibles:45
          };
          // se verifica cada lugar existene en el autobus
          for (var i = 0; i < 45; i++) {
            lugares[b.destino+'-'+b.nombre].lista.push({
              asiento:i+1,
              estatus:'disponible',
              nombre:'',
              usuario:'',
              corrida:b.id_corrida
            });
            // si el boleto fue comprado lo marca como ocupado y carga la informacion
            for (boleto of boletosVendidos.detalle) {
              if(boleto.corrida===b.id_corrida && boleto.asiento===i+1){
                lugares[b.destino+'-'+b.nombre].lista[i]={
                  asiento:i+1,
                  estatus:'ocupado',
                  nombre:boleto.nombre,
                  usuario:boleto.id_vendedor,
                  corrida:boleto.corrida
                };
                break;
              }
            }
          }
          // este ciclo hace el calculo de cuantos asientos hay disponibles
          for (ocupados of boletosVendidos) {
            if(ocupados.corrida===b.id_corrida){
              lugares[b.destino+'-'+b.nombre].disponibles=45-ocupados.vendidos;
            }
          }
          // se crea un manejador para cada corrida
          bus[b.destino+'-'+b.nombre]=io.of('/socket/bus/'+b.destino+'-'+b.nombre)
          .on('connection',function(socket){
            // se crea un id equivalente a b.destino+'-'+b.nombre
            let busID = this.name.split('/')[3];
            bus[busID].id=b.id_autobus;
            console.log('Se conecto un nuevo cliente a /bus/'+busID);
            // el evento lugares emite la informacion de los asientos de cada autobus
            socket.emit('lugares',lugares[busID])
            socket.on('pedir boleto',(lugar)=>{
              connection.query("SELECT b.id_boleto,b.nombre,b.asiento,d.nombre 'destino',b.corrida, c.costo,c.salida,u.nombre 'emisor', b.usuario 'id_vendedor', a.nombre 'autobus', c.autobus 'id_autobus' FROM boletos as b LEFT JOIN (corridas as c,destinos as d,usuarios as u, autobuses as a) ON (c.id_corrida=b.corrida AND d.id_destino=c.destino AND u.id_usuario=b.usuario AND a.id_autobus=c.autobus) WHERE b.corrida=:corrida AND b.asiento=:asiento order by b.creado desc limit 1",
              lugar, function(err, rows, fields){
                if(rows && rows.length===1){
                  console.log(rows);
                  socket.emit('boleto',rows[0]);
                }
              }
            )
            })
            // el evento actualizacion recibe el lugar que fue modificado
            socket.on('actualizacion',(data)=>{
              console.log(data);
              switch (data.lugar.estatus) {
                case 'reservado':
                // si es estatus es reservado disminulle en 1 los lugares disponibles
                lugares[busID].disponibles=lugares[busID].disponibles-1;
                break;
                case 'ocupado':
                // si el estado es ocupado, guarda la informacion en la BD
                let info = {
                  usuario:data.lugar.usuario,
                  corrida:bus[busID].data.id_corrida,
                  nombre:data.lugar.nombre,
                  asiento:data.lugar.asiento
                };
                console.log(info);
                connection.query('insert into boletos(`usuario`,`corrida`,`nombre`,`asiento`) values (:usuario,:corrida,:nombre,:asiento)',
                info,function(err,rows,fields){
                  if(err) console.log(err);
                })
                break;
                case 'disponible':
                // si el estado ahora es disponible aumenta en 1 el numero de lugares
                lugares[busID].disponibles=lugares[busID].disponibles+1;
                break;
              }
              // se actualiza la informacion en el arreglo que contiene todos los lugares de el autobus en cuestion
              lugares[busID].lista[data.index]=data.lugar;
              // al final de emite el evento lugares a todos los sockets (todos los clientes) que esten escuchando a este manejador
              bus[busID].emit('lugares',lugares[busID]);
              // si el estatus no es reservado se envia al socket de estadistica
              if(data.lugar.estatus!='reservado'){
                // se miten tambien al manejador encargado de la estadistica
                bus.list.emit('lugares',lugares);
              }
            })
            socket.on('disconnect', ()=>{
              console.log('Se desconecto un cliente de /bus/'+busID);
            });
          })
          // se guarda como referencia la informacion de esta corrida en el objeto data
          bus[b.destino+'-'+b.nombre].data=b;
        }
        server.listen(PORT,()=>{
          console.log('Aplicacion corriendo en el puerto %d',PORT);
        });
        module.exports=server;
        console.timeEnd('inicio en ');
      });
    })
  })
})();
